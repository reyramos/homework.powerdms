/**
 * # User-facing URL Router
 *
 * Specified what URLs are available to a user, and what controllers should be
 * assigned to each.
 */
angular.module('app').config(
	[ '$routeProvider'
	, '$httpProvider'
	, '$locationProvider'
	, function(
		  $routeProvider
		, $httpProvider
		, $locationProvider
		) {


		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];

		var initializeData = function() {

			var wrapper =
				[ 'appDataService'
					, function
					  ( appDataService ){

					return appDataService.initApplication();

				}];

			return { 'initializeData': wrapper };
		};

		$routeProvider
		.when
		(	'/'
		,	{
				   templateUrl: '/views/index.html'
				 , controller: 'storeFrontController'
				 , resolve: initializeData()
			 }
		)
		.when
		(	'/search/:query'
			,	{
				 templateUrl: '/views/index.html'
				 , controller: 'storeFrontController'
				 , resolve: initializeData()
			 }
		)
		.when
		(	'/request/:request'
			,	{
				 templateUrl: '/views/index.html'
				 , controller: 'storeFrontController'
				 , resolve: initializeData()
			 }
		)

		.otherwise({redirectTo:'/'})


		/**
		 * ## HTML5 pushState support
		 *
		 * This enables urls to be routed with HTML5 pushState so they appear in a
		 * '/someurl' format without a page refresh
		 *
		 * The server must support routing all urls to index.html as a catch-all for
		 * this to function properly,
		 *
		 * The alternative is to disable this which reverts to '#!/someurl'
		 * anchor-style urls.
		 */
		$locationProvider.html5Mode(true)
	}])


