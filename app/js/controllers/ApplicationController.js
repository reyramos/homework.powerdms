/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController',
	[
		 '$scope'
		, '$log'
		, 'appDataService'
		, function
		(
			 $scope
			, $log
			, appDataService
			) {


		$scope.displayCartItems = false;
		$scope.cartItems = appDataService.cartItems
		$scope.cartTotal = appDataService.cartTotal


		$scope.$watch
		( function(){
			  return appDataService.cartTotal;
		  }
			, function( cartTotal ){
			  $scope.safeApply( function () {
				  $scope.cartTotal = cartTotal
			  } )
		  }
		);

		$scope.safeApply = function ( fn ) {
			var phase = this.$root.$$phase;
			if (phase == '$apply' || phase == '$digest') {
				if (fn && (typeof(fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply( fn );
			}
		};

		$scope.itemRemove = function(item, $index){
			//update cart by removing total
			appDataService.cartTotal -= Number(item.price)
			$scope.cartItems.splice($index, 1)

		}


	}]
)
