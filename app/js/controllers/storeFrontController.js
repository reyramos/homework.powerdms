/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'storeFrontController',
	[
		'$scope'
		, '$log'
		, 'appDataService'
		, '$routeParams'
		, 'enumsService'
		, function
			(
				$scope
				, $log
				, appDataService
				, $routeParams
				, enumsService
				) {


		$scope.shelves = appDataService.results;


		var initialize = function(){

			//if the user places any query search on the url
			if($routeParams.query || $routeParams.q){
				$scope.query = $routeParams.query || $routeParams.q;
			}

			if($routeParams.request){
				appDataService.getRequestUrl(enumsService.getProducts+"?q="+$routeParams.request )
			}

		}()

		/**
		 * Adds item to cartItem object
		 * @param item
		 */
		$scope.addToCart = function(item, $index){
			item.index = $index; //just identifier

			var addToCart = true;
			for(var i in appDataService.cartItems){
				if(appDataService.cartItems[i].index == $index){
					addToCart = false;
				}
			}

			//check that we dont dups
			if(addToCart){
				appDataService.cartItems.push(item)
				appDataService.cartTotal += Number(item.price)
			}

		}



	}]
)
