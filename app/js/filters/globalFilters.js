/**
 * # Filters
 *
 * Helpers to be used in templates to format data displayed to a user
 */

angular.module('app').filter
( 'range'
	, function(){
	  return function(arr, lower, upper){
		  for (var i = lower; i <= upper; i++) arr.push(i)
		  return arr
	  }
  }
)

angular.module('app').filter
( 'JSONtoString'
	, function(){
	  return function(obj){
		  try{
			  return JSON.stringify(obj);
		  }
		  catch(error){
			  return JSON.stringify(error);
		  }
	  }
  }
)

angular.module('app').filter
	( 'truncate'
		, function (){
			return function (text, length, end) {
				if (isNaN(length))
					length = 10;

				if (end === undefined)
					end = "...";

				if (text.length <= length || text.length - end.length <= length) {
					return text;
				}
				else {
					return String(text).substring(0, length-end.length) + end;
				}

			};
		}
	)

angular.module('app').filter
('maxChar'
	, function(){
	 return function (text, length, end) {

		 if (isNaN(length))
			 length = 10;

		 if (end === undefined)
			 end = "...";

		 if(typeof(text) == "undefined")
			 return;

		 if (text.length <= length || text.length - end.length <= length) {
			 return text;
		 }

		 else {
			 return String(text).substring(0, length-end.length) + end;
		 }

	 };
 }
)

angular.module('app').filter
('rightSubstring'
	, function(){
	 return function (value, length){
		 string = '' + value
		 return string.substring(string.length - length)
	 }
 }
)

angular.module('app').filter
('leadingZeros'
	, function(){
	 return function (value, length){
		 string = '' + value
		 while (string.length < length) string = '0' + string
		 return string
	 }
 }
)

angular.module('app').filter
( 'camelCase',
  function (){
	  return function (text){
		  var str = text.toLowerCase();
		  var arr = str.split(" ");
		  str = arr[0];
		  for( i in arr){
			  if(i > 0){
				  str += arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
			  }
		  }
		  return str
	  }
  }
)