// Require JS  Config File
require( {
			 baseUrl: '/js',
			 paths: {
				 'angular': '../lib/angular/index'
				 , 'angular-resource': '../lib/angular-resource/index'
				 , 'angular-route': '../lib/angular-route/index'
				 , 'angular-cookies': '../lib/angular-cookies/index'
				 , 'angular-sanitize': '../lib/angular-sanitize/index'
				 , 'angular-animate': '../lib/angular-animate/index'
				 , 'angular-touch': '../lib/angular-touch/index'
				 , 'jquery': '../lib/jquery/dist/jquery'
				 , 'jquery-ui': '../lib/jquery-ui/index'
				 , 'sockjs': '../lib/sockjs/index'

			 }, map: {
				 '*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
			 }, shim: {
				 'app': { 'deps': [
					   'angular'
					 , 'angular-route'
					 , 'angular-resource'
					 , 'angular-sanitize'
					 , 'angular-animate'
					 , 'angular-cookies'
					 , 'angular-touch'

				 ]}
				 , 'angular-route': { 'deps': ['angular', 'jquery'], exports: 'angular' }
				 , 'angular-resource': { 'deps': ['angular'] }
				 , 'angular-cookies': { 'deps': ['angular'] }
				 , 'angular-sanitize': { 'deps': ['angular'] }
				 , 'angular-animate': { 'deps': ['angular'] }
				 , 'angular-touch': { 'deps': ['angular'] }
				 , 'jquery': {
					 init: function ( $ ) {
						 return $.noConflict( true );
					 },
					 exports: 'jquery'
				 }
				 , 'jquery-ui': { 'deps': ['jquery'] }
				 , 'routes': { 'deps': [
					 'app'
				 ]}
				, 'filters/globalFilters': { 'deps': ['app'] }
				, 'controllers/ApplicationController': {
					 'deps': [
						 'app'
				 ]}
				 , 'controllers/storeFrontController': {
					 'deps': [
						  'app'
						 ,'services/appDataService'
						 ,'services/enumsService'
					 ]}
				 , 'services/appDataService': { 'deps': [
					 'app','services/enumsService'
				 ]}
				 , 'services/enumsService': { 'deps': [
					 'app'
				 ]}
				}
			 }
		, [
			 'require'
			 , 'routes'
			 , 'filters/globalFilters'
			 , 'controllers/ApplicationController'
			 , 'controllers/storeFrontController'
			 , 'services/appDataService'
			 , 'services/enumsService'
		 ]
	, function ( require ) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);