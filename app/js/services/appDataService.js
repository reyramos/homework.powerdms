/**
 * # Global string enumerations file
 *
 * Any strings that are used as a part of application logic should be defined
 * here, and their object key be referenced instead.
 *
 * This is especially useful in cases like $on event listeners where a typo in
 * the name would not ever error, but would simply never catch.
 *
 * By the extra work of defining enumerations for all strings and using those
 * enumerations avoids this by at least throwing undefined argument errors.
 *
 */

	// @TODO: Move this to enums directory and rename to globalEnums.
angular.module('app').factory
	( 'appDataService',[
		'$q'
		, '$log'
		, '$http'
		, 'enumsService'
		, '$templateCache'
		, function(
			$q
			, $log
			, $http
			, enumsService
			, $templateCache
			) {


			var jsonRequest = enumsService.requestTypes

			var appDataService = {
				cartItems:[],
				cartTotal:0
			};

			appDataService.initApplication = function(){
				var defer = $q.defer()

				getRequestUrl().then(function(){
					defer.resolve();
				})

				return defer.promise;
			}

			appDataService.getRequestUrl = function(url){
				var defer = $q.defer(),results;
				$http.get(url).success(function(data){
					results = cleanData(data);
					appDataService.results = results
					defer.resolve( results );
				})
				return defer.promise;
			}


			function getCORSRequest(){
				var xhr = createCORSRequest('GET', jsonRequest.getProducts);
				if (!xhr) {
					throw new Error('CORS not supported');
				}
				console.log(xhr)
				xhr.onload = function() {
					var responseText = xhr.responseText;
					console.log(responseText);
					// process the response.
				};

				xhr.onerror = function() {
					console.log('There was an error!');
				};
			}


			function createCORSRequest(method, url) {
				var xhr = new XMLHttpRequest();

				xhr.withCredentials = true;


				if ("withCredentials" in xhr) {

					// Check if the XMLHttpRequest object has a "withCredentials" property.
					// "withCredentials" only exists on XMLHTTPRequest2 objects.
					xhr.open(method, url, true);

				} else if (typeof XDomainRequest != "undefined") {

					// Otherwise, check if XDomainRequest.
					// XDomainRequest only exists in IE, and is IE's way of making CORS requests.
					xhr = new XDomainRequest();
					xhr.open(method, url);

				} else {

					// Otherwise, CORS is not supported by the browser.
					xhr = null;

				}
				return xhr;
			}


			/**
			 * Gets the json file from $http.get(url) request, once success data is gathered it will update
			 * appDataService with new keys based on request url file name
			 * @param file
			 * @returns {exports.pending.promise|*|promise|defer.promise|promiseContainer.promise|a.fn.promise}
			 */
			function getRequestUrl (){

				var deferred  = $q.defer(),results;

				var handler = []
				for(var i in jsonRequest){
					handler.push($http({method: "GET", url: jsonRequest[i], cache: $templateCache}))
				}

				$q.all( handler ).then(function(arrayOfResults){

					for(var i in arrayOfResults){
						if(arrayOfResults[i].status == 200){
							results = cleanData(arrayOfResults[i].data);
						}
					}
					appDataService.results = results

					deferred.resolve( results );

				}, function(data){
					$log.log('failed >',data);
				});

				return deferred .promise;
			}


			function cleanData(data){
				var results = [];
				for(var k in data){
					results.push(data[k])
				}
				return results;
			}




			return appDataService;
		}]
	)