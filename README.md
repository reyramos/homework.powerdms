## PowerDMS JavaScript Project ##

This site demo is build on HTML5 Angula Js application.
Requirement to run the uncompile version will need Nodejs install, Bower, Grunt and PhantomJS.  The distribution version can be found here
<http://homework.powerdms.myphpdelights.com/>

## Main features

- /root/ gathers sites api of all products/
- /search/search+items will gather items from api/products/ , this ability does not make the api call
- Force a request to the api /request/search+item, this will call the api to seach for item, return the results
- Add items to cart, update totals and remove item from cart

## Development Environment Set-up

  1. Install Node.JS
	Use your system package manager (brew,port,apt-get,yum etc)

  2. Install global Bower, Grunt and PhantomJS commands, this step is not necessary,
  because once you perform grunt build it will install all the necessary files from package.json

  ```bash
	sudo npm install -g grunt-cli bower phantomjs
  ```

  3. Install npm packages from package.json
  ```bash
	sudo npm install
  ```

  4. Install bower packages from bower.json
   ```bash
 	sudo bower install --allow-root
   ```

## Development Work-flow ##
  1. Build Development/Distribution
  ```bash
	grunt
	or
	grunt build
  ```

  2. Run server
  ```bash
   	grunt server
  ```

  3. Open you browser to http://localhost:9000

## Browser support
- Chrome 7+
- IE 10+
