<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 3/21/14
 * Time: 11:39 PM
 *
 * While still investigating Cross-Domain with Cross-Origin Resource, a workaround it to gather the necessary request through PHP.
 *
 * In order for CORS to work with client and server, the server will also need CORS credential allow:
 * Access-Control-Allow-Credentials: true
 *
 *
 */



$param = isset($_REQUEST['q'])?"?q=".$_REQUEST['q']:"";
$json = file_get_contents("http://homework.powerdms.com/products/$param");

echo $json;
