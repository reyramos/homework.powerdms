<?php
/**
 * Created by PhpStorm.
 * User: redroger
 * Date: 2/19/14
 * Time: 8:20 PM
 */

namespace MyApp;


use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
	protected $clients;

	public function __construct() {
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn) {
		// Store the new connection to send messages to later
		$this->clients->attach($conn);

		echo "New connection! ({$conn->resourceId})\n";

		if(count($this->clients) == 1){
			$array = array("msg"=>"Welcome to Tic Tac Toe, waiting for player",
				"status" => false);



			$conn->send(json_encode($array));
		}else
			if(count($this->clients) == 2){

				$array = array("msg"=>"Welcome to Tic Tac Toe",
					"status" => true);

			$conn->send(json_encode($array));
			$this->onMessage($conn, json_encode($array));
		}
	}

	public function onMessage(ConnectionInterface $from, $msg) {
		$numRecv = count($this->clients) - 1;
		echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
			, $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

		foreach ($this->clients as $client) {
			if ($from !== $client) {
				// The sender is not the receiver, send to each client connected
				$client->send($msg);
			}
		}
	}

	public function onClose(ConnectionInterface $conn) {
		// The connection is closed, remove it, as we can no longer send it messages
		$this->clients->detach($conn);

		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}
}